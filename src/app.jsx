/*
 * This file is part of Cockpit.
 *
 * Copyright (C) 2017 Red Hat, Inc.
 *
 * Cockpit is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * Cockpit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Cockpit; If not, see <http://www.gnu.org/licenses/>.
 */

import cockpit from 'cockpit';
import React from 'react';
import { Alert, Card, CardTitle, CardBody, List, ListItem, Label } from '@patternfly/react-core';

const _ = cockpit.gettext;

export class Application extends React.Component {
    zerotier_bin = '/usr/sbin/zerotier-cli';
    constructor() {
        super();
        this.state = {
            online: false,
            address: _(""),
            version: _(""),
            networks: []
        };
        cockpit.script(this.zerotier_bin + ' -j info', { superuser: "yes" })
                .then(data => {
                    const json_data = JSON.parse(data);
                    this.setState({
                        online: json_data.online,
                        address: json_data.address,
                        version: json_data.version
                    });
                })
                .catch(error => {
                    console.log(error.message);
                });

        cockpit.script(this.zerotier_bin + ' -j listnetworks', { superuser: "yes" })
                .then(data => {
                    const json_data = JSON.parse(data);
                    this.setState({
                        networks: json_data
                    });
                })
                .catch(error => {
                    console.log(error.message);
                });
    }

    render() {
        return (
            <Card>
                <CardTitle>Zerotier One status by Dave Rohlicek</CardTitle>
                <CardBody>
                    <Alert
                        variant={this.state.online ? 'success' : 'warning'}
                        title={ this.state.online ? cockpit.format(_("ZeroTier One $0 [ $1 ]"), this.state.version, this.state.address) : 'ZeroTier One not running'}
                    />
                    <br />
                    <Card>
                        <CardTitle>Networks member:</CardTitle>
                        <CardBody>
                            <List isPlain isBordered>
                                { this.state.networks.map((item) => <ListItem key={item.nwid}>{ item.name } - <Label color={item.status === 'OK' ? 'green' : 'red'}>{ item.status }</Label> - { item.assignedAddresses[0] }</ListItem>) }
                            </List>
                        </CardBody>
                    </Card>
                </CardBody>
            </Card>
        );
    }
}
